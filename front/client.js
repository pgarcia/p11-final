var pc = null;

function negotiate(videoName) {
    pc.addTransceiver('video', { direction: 'recvonly' });
    pc.addTransceiver('audio', { direction: 'recvonly' });

    return pc.createOffer().then((offer) => {
        return pc.setLocalDescription(offer);
    }).then(() => {
        // wait for ICE gathering to complete
        return new Promise((resolve) => {
            if (pc.iceGatheringState === 'complete') {
                resolve();
            } else {
                const checkState = () => {
                    if (pc.iceGatheringState === 'complete') {
                        pc.removeEventListener('icegatheringstatechange', checkState);
                        resolve();
                    }
                };
                pc.addEventListener('icegatheringstatechange', checkState);
            }
        });
    }).then(() => {
        var offer = pc.localDescription;
        console.log('Sending offer:', offer);
        return fetch('/offer', {
            body: JSON.stringify({
                sdp: offer.sdp,
                type: offer.type,
                video: videoName
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        });
    }).then((response) => {
        return response.json();
    }).then((answer) => {
        console.log('Received answer:', answer);
        return pc.setRemoteDescription(answer);
    }).then(() => {
        saveSDP(videoName);
    }).catch((e) => {
        alert(e);
    });
}

function startStream(videoName, videoTitle) {
    var config = {
        sdpSemantics: 'unified-plan'
    };

    if (document.getElementById('use-stun').checked) {
        config.iceServers = [{ urls: ['stun:stun.l.google.com:19302'] }];
    }

    pc = new RTCPeerConnection(config);

    // connect audio / video
    pc.addEventListener('track', (evt) => {
        if (evt.track.kind == 'video') {
            document.getElementById('video').srcObject = evt.streams[0];
        } else {
            document.getElementById('audio').srcObject = evt.streams[0];
        }
    });

    // Get the correct elements using the dynamic ID
    const startButton = document.getElementById(`start-${videoName}`);
    const stopButton = document.getElementById(`stop-${videoName}`);

    // Debugging logs
    console.log("Start button element:", startButton);
    console.log("Stop button element:", stopButton);

    // Ensure elements are not null
    if (startButton && stopButton) {
        startButton.style.display = 'none';
        negotiate(videoName).then(() => {
            stopButton.style.display = 'inline-block';
        });
    } else {
        console.error("Start or Stop button not found in the DOM");
    }
}

function stopStream(videoName) {
    const stopButton = document.getElementById(`stop-${videoName}`);
    const startButton = document.getElementById(`start-${videoName}`);

    // Ensure elements are not null
    if (stopButton && startButton) {
        stopButton.style.display = 'none';
        startButton.style.display = 'inline-block';
    } else {
        console.error("Start or Stop button not found in the DOM");
    }

    // close peer connection
    setTimeout(() => {
        pc.close();
        pc = null;
    }, 500);
}

function saveSDP(videoName) {
    if (pc && pc.localDescription) {
        console.log('Saving SDP:', pc.localDescription.sdp);
        return fetch('/save-sdp', {
            body: JSON.stringify({
                sdp: pc.localDescription.sdp,
                type: pc.localDescription.type,
                video: videoName
            }),
            headers: {
                'Content-Type': 'application/json'
            },
            method: 'POST'
        }).then((response) => {
            return response.json();
        }).then((result) => {
            console.log('SDP saved successfully:', result);
        }).catch((e) => {
            console.error('Error saving SDP:', e);
        });
    } else {
        console.error('PeerConnection is not established or SDP is not available');
    }
}
