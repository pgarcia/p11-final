import argparse
import asyncio
import json
import sys
import datetime
import os

def log_message(message):
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")[:-3]
    log_entry = f"{timestamp} {message}"
    sys.stderr.write(log_entry + "\n")

def save_sdp(sdp_message, sdp_type, client_addr):
    save_path = 'sdp_files'
    if not os.path.exists(save_path):
        os.makedirs(save_path)

    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")[:-3]
    filename = f"{sdp_type}_{client_addr[0]}_{client_addr[1]}_{timestamp}.sdp"
    file_path = os.path.join(save_path, filename)

    with open(file_path, 'w') as f:
        f.write(sdp_message)

    log_message(f"SDP {sdp_type} guardada en {file_path}")

clientlist = []
streamers_dict = {}
ficheros = []
mensaje_no_enviado = []
streamer_elegido = ""

class EchoServerProtocol:
    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))
        if message.split("-")[0] == "REGISTER STREAMER":
            for streamer in json.loads(message.split("-")[1]).keys():
                streamers_dict[streamer] = addr
            print(streamers_dict)
            ficheros.append(message.split("-")[1])
            log_message("Mensaje REGISTRO STREAMER recibido de " + str(addr))

        elif message == "LISTA":
            clientlist.append({"Nombre": len(clientlist) + 1, "Direccion": addr})
            print('Send %r to %s' % (ficheros, addr))
            self.transport.sendto(json.dumps(ficheros).encode(), addr)

        elif message.split(":")[0] == "Name":
            global streamer_elegido
            streamer_elegido = message.split(":")[1]

        elif message.split('"')[-2] == "offer":
            log_message("Mensaje de oferta SDP recibido de" + str(addr))
            save_sdp(message, 'offer', addr)
            try:
                self.transport.sendto(message.encode(), streamers_dict[streamer_elegido])
                log_message("Mensaje de oferta SDP enviado a " + str(streamers_dict[streamer_elegido]))
            except IndexError:
                self.transport.sendto(
                    "No hay servidores disponibles, se enviara el mensaje cuando se abra un servidor".encode(),
                    clientlist[-1]["Direccion"])
                mensaje_no_enviado.append(message)
        elif message.split('"')[-2] == "answer":
            log_message("Mensaje de respuesta SDP recibido de " + str(addr))
            save_sdp(message, 'answer', addr)
            self.transport.sendto(message.encode(), clientlist[-1]["Direccion"])
            log_message("Mensaje de respuesta SDP enviada a" + str(clientlist[-1]["Direccion"]))

async def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("signal_port", type=int, help="UDP port to listen for signaling messages")
    args = parser.parse_args()
    port = args.signal_port
    log_message("Comenzando")
    loop = asyncio.get_running_loop()
    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoServerProtocol(),
        local_addr=('127.0.0.1', port))

    try:
        await asyncio.sleep(3600)
    finally:
        transport.close()

if __name__ == "__main__":
    asyncio.run(main())
