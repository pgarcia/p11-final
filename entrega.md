# ENTREGA CONVOCATORIA JUNIO
###### NOMBRE: Paula García Rodríguez -- CORREO DE LA UNIVERSIDAD: p.garciar.2019@alumnos.urjc.es

## PARTE BÁSICA
Este programa implementa un sistema de transmisión de video en tiempo real utilizando WebRTC, la parte basica consta de todos los programas requeridos en el enunciado del proyecto.
* Primero, el sistema de señalización se inicia en el puerto de señal y se configuran los cuatro transmisores de video que transmiten videos de ciudades europeas.
* Durante la inicialización de los transmisores, estos envían mensajes de registro al sistema de señalización, proporcionando su información.
* Se configura el servidor frontal en el puerto HTTP y se abre el navegador con la URL aportada.
* Cuando un usuario hace clic en el botón "start" para reproducir cualquiera de los cuatro videos, el archivo HTML envía una solicitud de oferta (offer) al servidor frontal, que la retransmite al sistema de señalización. El sistema de señalización pasa la oferta al transmisor correspondiente, que responde con una respuesta (answer). Esta respuesta se envía de vuelta al servidor frontal y luego al navegador, estableciendo una conexión WebRTC que permite la reproducción del video en el navegador.
* ###### SOLO FUNCIONA EN EL NAVEGADOR GOOGLE CHROME

## PARTE ADICIONAL
* Información adicional para los ficheros: Cuando los streamers se registren, enviarán al servidor de señalización información adicional. Esta información incluye un título y un resumen en texto del fichero. Esta información se almacena en el directorio del servidor de señalización, se la pasa al servidor frontal cuando éste le pida un listado, para así mostrársela a los navegadores.


###### ENLACE AL VIDEO DE DEMOSTRACIÓN DEL PROYECTO:

* https://youtu.be/6hPU7A6hlJE